/**
 * Created by Fanjiang Huang on 2016/7/2.
 */

$(function(){

    bannerSize($('.store-inf'));

    //收藏会员卡
    $('.collect').click(function(){
        if ($(this).attr('collect') == 0) {
            var arr = new Array();
            arr.push('<span class="path1"></span>');
            arr.push('<span class="path2"></span>');
            arr.push('<span class="path3"></span>');
            arr.push('<span class="path4"></span>');
            $(this).removeClass('icon-card');
            $(this).addClass('icon-card-on');
            $(this).html(arr.join(''));
            $(this).attr('collect', '1');
        } else {
            $(this).html('');
            $(this).addClass('icon-card');
            $(this).removeClass('icon-card-on');
            $(this).attr('collect', '0');
        }
    });

    //选项卡
    var tabsSwiper = new Swiper('.tabs-switch-container',{
        autoHeight: true,
        speed:500,
        onSlideChangeStart: function(){
            $('.tabs-store .active').removeClass('active');
            $('.tabs-store li').eq(tabsSwiper.activeIndex).addClass('active');
        }
    });

    $('.tabs-store li').on('touchstart mousedown',function(e){
        e.preventDefault();
        $('.tabs-store .active').removeClass('active');
        $(this).addClass('active');
        tabsSwiper.slideTo($(this).index());
    });

    $('.tabs-store li').click(function(e){
        e.preventDefault();
    });

    //选项卡内容高度限制
    var tabsCon = $('.tabs-switch-container');
    var tabList = $('.tabs-switch-slide');
    var headerHeight = $('header').height();
    var tabsHeight = $('.tabs-store').height();
    var footerHeight = $('footer').height();
    var winHeight = $(window).height();

    tabsCon.css('height', winHeight - headerHeight - tabsHeight - footerHeight + 1);
    tabList.css('height', winHeight - headerHeight - tabsHeight - footerHeight + 1);

    //选项卡滚动到头部悬停
    $(window).scroll(function(){
        var section = $('section');
        var tabList = $('.tabs-switch-slide');
        var winPosition = $(window).scrollTop();
        var bannerPosition = $('.store-inf').height();

        if (winPosition >= bannerPosition) {
            tabList.css('overflow-y', 'auto');
        } else {
            tabList.css('overflow-y', 'hidden');
        }
    });

});

$(window).resize(function(){
    bannerSize($('.store-inf'));
});

//广告轮播尺寸等比缩放
function bannerSize (obj) {
    var imgWidth = $(window).width();
    var imgHeight = imgWidth * (320 / 750);
    obj.height(imgHeight);
}