/**
 * Created by Fanjiang Huang on 2016/6/24.
 */

$(function(){

    //富文本字体控制
    richText($('.single-action-word p span'));

    /*弹出层*/
    $('.icon-select').click(function(e){
        if ($('.select-dialog').is(':hidden')) {
            $('.select-dialog').fadeIn();
        } else {
            $('.select-dialog').fadeOut();
        }
        e.stopPropagation();
    });

    $('.select-dialog').click(function(){
        $('.select-dialog').fadeOut();
    });

    $('.select-bg').click(function(e){
        e.stopPropagation();
    });

});


//富文本字体控制(如果是异步的页面，请在接口内调用)
function richText (obj) {
    obj.each(function () {
        var dataLevel = $('html').attr('data-dpr');
        var _this = $(this);
        var fontSize =  parseInt(_this.attr('style').match(/(?:font-size:)*(\d+\d*)/));

        if (dataLevel == 2) {
            _this.css('font-size',2*fontSize);
        } else if (dataLevel == 3) {
            _this.css('font-size',3*fontSize);
        }
    });
}