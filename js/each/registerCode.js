/**
 * Created by Fanjiang Huang on 2016/6/29.
 */


/*短信重发倒数*/
var count = 60;
function setTime(btn){
    if (count == 0) {
        btn.removeAttribute("disabled");
        btn.value = '重新获取验证码';
        count = 60;
    } else {
        btn.setAttribute("disabled", true)
        btn.value = '接收短信大约需要'+ count +'秒钟';
        count--;
        setTimeout(function(){
            setTime(btn);
        },1000);
    }
}

$(function(){

    /*短信重发倒数*/
    $('.user-time input').click(function(){
        setTime(this);
    });

    /*进入页面自动触发倒数*/
    $('.user-time input').trigger("click");
});

