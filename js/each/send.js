/**
 * Created by Fanjiang Huang on 2016/6/26.
 */


$(function(){

    /*文本框自动伸缩*/
    autosize(document.querySelectorAll('textarea'));

    /*日历插件*/
    var currYear = (new Date()).getFullYear();
    var opt={};
    opt.date = {preset : 'date'};
    opt.datetime = {preset : 'datetime'};
    opt.time = {preset : 'time'};
    opt.default = {
        theme: 'mobiscroll', //皮肤样式
        display: 'bottom', //显示方式
        mode: 'scroller', //日期选择模式
        dateFormat: 'yyyy-mm-dd',
        lang: 'zh',
        showNow: true,
        nowText: "今天",
        rows:5,//滚动区域内的行数
        startYear: currYear - 50, //开始年份
        endYear: currYear + 10, //结束年份
        headerText:function(valueText){return "年/月/日 时/分"}
    };

    $('.datetime').mobiscroll($.extend(opt['datetime'], opt['default']));

});