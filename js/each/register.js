/**
 * Created by Fanjiang Huang on 2016/6/29.
 */

$(function () {
    $('#register').click(function () {
        $('.sure-dialog').fadeIn();
    });

    //确认框调用
    comfirm(function (value) {
        if (value == false) {
            alert(0);
        } else {
            alert(1);
        }
    });
});

//确认框构建
function comfirm(callback) {

    $('.sure-dialog-btn a:first-child').click(function(){
        $('.sure-dialog').fadeOut();
        if (typeof (callback) == 'function') {
            callback(false);
        }
    })

    $('.sure-dialog-btn a:last-child').click(function(){
        $('.sure-dialog').fadeOut();
        if (typeof (callback) == 'function') {
            callback(true);
        }
    })
}