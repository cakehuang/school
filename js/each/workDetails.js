/**
 * Created by Fanjiang Huang on 2016/7/1.
 */

$(function(){

    //加载脚部
    $('footer').load('footerJoin.html', function () {

        //页面载入后，如果不能操作报名
        if ($('.work-details-btn').hasClass('btn-grey')) {
            $('.work-details-btn button')
                .attr('disabled',true);
        }
        
    });

    //富文本字体控制
    richText($('.details-con p span'));


    //点击报名，弹出确认框
    $('footer').on('click', '.work-details-btn', function () {
        var _this = $(this);
        if (!_this.hasClass('btn-grey')) {
            $('.sure-dialog').fadeIn();
        }
    });

    //确认框调用
    comfirm(function (value) {
        if (value == false) {
            $('.sure-dialog').fadeOut();
        } else {
            $('.work-details-btn').addClass('btn-grey');
            $('.sure-dialog').fadeOut();
            $('.work-details-btn button')
                .attr('disabled',true)
                .text('已报名');
        }
    });

    //点击分享
    $('.icon-share').click(function () {
        $('.share-dialog').fadeIn();
    });

    $('.share-dialog').click(function () {
        $(this).fadeOut();
        $('#jiathis_weixin_modal').remove();
    });

    $('.share-main').click(function (event) {
        event.stopPropagation();
    });

    //微信二维码样式
    $('.jiathis_button_weixin').click(function () {
        setInterval(function(){
            if ($('#jiathis_weixin_modal').length > 0) {
                $('#jiathis_weixin_modal').css({
                    'width':'6rem',
                    'height':'auto',
                    'top':'20%',
                    '-webkit-transform':'translate(-50%,0)',
                    '-moz-transform':'translate(-50%,0)',
                    '-ms-transform':'translate(-50%,0)',
                    '-o-transform':'translate(-50%,0)',
                    'transform':'translate(-50%,0)',
                    'margin':'0',
                    'border-radius':'0',
                    '-webkit-box-shadow':'rgba(0, 0, 0, 0.2) 0 0.08rem 0.133rem',
                    '-moz-box-shadow':'rgba(0, 0, 0, 0.2) 0 0.08rem 0.133rem',
                    'box-shadow':'rgba(0, 0, 0, 0.2) 0 0.08rem 0.133rem'
                });

                $('.jiathis_modal_header').css({
                    'padding':'0.133rem 0.4rem'
                });

                $('#jiathis_weixin_h3').css({
                    'line-height':'0.833rem'
                });


                $('.jiathis_modal_header a').css({
                    'margin-top':'0',
                    'line-height':'0.833rem',
                    'display':'block'
                });

                if ($('html').attr('data-dpr') == 1) {
                    $('.jiathis_modal_header a').css({
                        'font-size':'24px'
                    });
                } else if ($('html').attr('data-dpr') == 2) {
                    $('.jiathis_modal_header a').css({
                        'font-size':'48px'
                    });

                } else if ($('html').attr('data-dpr') == 3) {
                    $('.jiathis_modal_header a').css({
                        'font-size':'72px'
                    });
                }

                $('.jiathis_modal_body').css({
                    'height':'6rem',
                    'width':'6rem'
                });

                $('.jiathis_modal_body p').css({
                    'height':'6rem',
                    'width':'6rem',
                    'padding':'0.267rem',
                    '-webkit-box-sizing':'border-box',
                    '-moz-box-sizing':'border-box',
                    'box-sizing':'border-box'
                });

                $('.jiathis_modal_body p img').attr({
                    'height':'100%',
                    'width':'100%'
                });

                $('.jiathis_modal_body p img').css({
                    'margin-top':'0',
                    'vertical-align':'middle'
                });

                $('.jiathis_modal_footer ').remove();

            }
        },50);
    });
});

//富文本字体控制(如果是异步的页面，请在接口内调用)
function richText (obj) {
    obj.each(function () {
        var dataLevel = $('html').attr('data-dpr');
        var _this = $(this);
        var fontSize =  parseInt(_this.attr('style').match(/(?:font-size:)*(\d+\d*)/));

        if (dataLevel == 2) {
            _this.css('font-size',2*fontSize);
        } else if (dataLevel == 3) {
            _this.css('font-size',3*fontSize);
        }
    });
}

//确认框构建
function comfirm(callback) {

    $('.sure-dialog-btn a:first-child').click(function(){
        if (typeof (callback) == 'function') {
            callback(false);
        }
    });

    $('.sure-dialog-btn a:last-child').click(function(){
        if (typeof (callback) == 'function') {
            callback(true);
        }
    });
}