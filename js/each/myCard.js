/**
 * Created by Fanjiang Huang on 2016/6/28.
 */

$(function(){
    $('.card-list li').click(function(){
        $('.card-dialog').fadeIn();
    });

    $('.card-dialog').click(function(){
        $(this).fadeOut();
    });
});
