/**
 * Created by Fanjiang Huang on 2016/6/29.
 */

$(function(){



    /*日历插件*/
    var currYear = (new Date()).getFullYear();
    var opt={};
    opt.date = {preset : 'date'};
    opt.datetime = {preset : 'datetime'};
    opt.time = {preset : 'time'};
    opt.default = {
        theme: 'mobiscroll', //皮肤样式
        display: 'bottom', //显示方式
        mode: 'scroller', //日期选择模式
        dateFormat: 'yyyy-mm-dd',
        lang: 'zh',
        showNow: true,
        nowText: "今天",
        startYear: currYear - 50, //开始年份
        endYear: currYear + 10 ,//结束年份
        rows:5,//滚动区域内的行数
        headerText:function(valueText){return "年/月/日"}
    };

    $('.date').mobiscroll($.extend(opt['date'], opt['default']));

    /*地区选择*/

    $('.demo_treelist').mobiscroll().treelist({
        theme: 'mobiscroll',
        mode: 'scroller',
        display: 'bottom',
        lang: 'zh',
        rows:5,//滚动区域内的行数
        placeholder: '请选择省/市/区',
        headerText:function(valueText){return "省/市/区"}
    });

    $('.demo-cont').show();

});