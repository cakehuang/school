/**
 * Created by fanjiang Huang on 2016/6/21.
 */

$(function(){

    //富文本字体控制
    richText($('.single-action-word p span'));

    //轮播广告尺寸缩放
    bannerSize($('.banner .swiper-container'));

    //载入脚部
    var nav = $('footer');
    nav.load('footer.html', function(){
        $(this).children().find('a:first-child').addClass('nav-on');
    });

    //轮播广告
    var swiper = new Swiper('.banner .swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        centeredSlides: true,
        autoplayDisableOnInteraction: false,
        loop: true
    });

    //选项卡
    $('.tabs-line').css('width', $('.active').width());
    $('.tabs-line').css('left', $('.active').offset().left);

    var tabsSwiper = new Swiper('.tabs-switch-container',{
        autoHeight: true,
        speed:500,
        onSlideChangeStart: function(){
            $('.tabs .active').removeClass('active');
            $('.tabs li').eq(tabsSwiper.activeIndex).addClass('active');

            $('.tabs-line').css('left', $('.tabs li').eq(tabsSwiper.activeIndex).offset().left);
            $('.tabs-line').css('width', $('.tabs li').eq(tabsSwiper.activeIndex).width());
        }
    });

    $('.tabs li').on('touchstart mousedown',function(e){
        e.preventDefault();
        $('.tabs .active').removeClass('active');
        $(this).addClass('active');
        tabsSwiper.slideTo($(this).index());
        $('.tabs-line').css('left', $(this).offset().left);
        $('.tabs-line').css('width', $(this).width());
    });

    $('.tabs li').click(function(e){
        e.preventDefault();
    });

    //选项卡内容高度限制
    var tabsCon = $('.tabs-switch-container');
    var tabList = $('.tabs-switch-slide');
    var headerHeight = $('header').height();
    var tabsHeight = $('.tabs').height();
    var footerHeight = $('footer').height();
    var winHeight = $(window).height();

    tabsCon.css('height', winHeight - headerHeight - tabsHeight - footerHeight + 1);
    tabList.css('height', winHeight - headerHeight - tabsHeight - footerHeight + 1);

    //选项卡滚动到头部悬停
    $(window).scroll(function(){
        var section = $('section');
        var tabList = $('.tabs-switch-slide');
        var winPosition = $(window).scrollTop();
        var bannerPosition = $('.banner').height();

        if (winPosition >= bannerPosition) {
            tabList.css('overflow-y', 'auto');
        } else {
            tabList.css('overflow-y', 'hidden');
        }
    });

    //商家种类
    var swiperH = new Swiper('.swiper-container-v', {
        pagination: '.swiper-pagination-v',
        slidesPerView: 3.6,
        freeMode: true
    });


});

$(window).resize(function(){

    //轮播广告尺寸缩放
    bannerSize($('.banner .swiper-container'));

    $('.tabs-line').css('width', $('.active').width());
    $('.tabs-line').css('left', $('.active').offset().left);

    var tabsCon = $('.tabs-switch .swiper-container');
    var tabList = $('.tabs-switch .content-slide');
    var headerHeight = $('header').height();
    var tabsHeight = $('.tabs').height();
    var footerHeight = $('footer').height();
    var winHeight = $(window).height();

    tabsCon.css('height', winHeight - headerHeight - tabsHeight - footerHeight);
    tabList.css('height', winHeight - headerHeight - tabsHeight - footerHeight);

});

//广告轮播尺寸等比缩放
function bannerSize (obj) {
    var imgWidth = $(window).width();
    var imgHeight = imgWidth * (250 / 750);
    obj.height(imgHeight);
}

//富文本字体控制(如果是异步的页面，请在接口内调用)
function richText (obj) {
    obj.each(function () {
        var dataLevel = $('html').attr('data-dpr');
        var _this = $(this);
        var fontSize =  parseInt(_this.attr('style').match(/(?:font-size:)*(\d+\d*)/));

        if (dataLevel == 2) {
            _this.css('font-size',2*fontSize);
        } else if (dataLevel == 3) {
            _this.css('font-size',3*fontSize);
        }
    });
}