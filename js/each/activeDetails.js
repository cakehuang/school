/**
 * Created by Fanjiang Huang on 2016/7/1.
 */

$(function(){

    //富文本字体控制
    richText($('.details-con p span'));

    $('footer').load('footerJoin.html', function () {

        //页面载入后，如果不能操作报名
        if ($('.work-details-btn').hasClass('btn-grey')) {
            $('.work-details-btn button')
                .attr('disabled',true);
        }

    });

    //点击报名，弹出确认框
    $('footer').on('click', '.work-details-btn', function () {
        var _this = $(this);
        if (!_this.hasClass('btn-grey')) {
            $('.sure-dialog').fadeIn();
        }
    });

    //确认框调用
    comfirm(function (value) {
        if (value == false) {
            $('.sure-dialog').fadeOut();
        } else {
            $('.work-details-btn').addClass('btn-grey');
            $('.sure-dialog').fadeOut();
            $('.work-details-btn button')
                .attr('disabled',true)
                .text('已报名');
        }
    });

    /*投诉*/
    $('.icon-more').click(function(){
        $('.dialog-complaint').fadeIn();
    });

    $('.dialog-complaint').click(function () {
        $(this).fadeOut();
    });

    $('.dialog-complaint > ul').click(function (e) {
        e.stopPropagation();
    })

    /*滚到到位置导航变颜色*/
    $(window).scroll(function(){

        var winScroll = $(window).scrollTop();
        var scrollHeight = $('.details-big-pic').height();

        if (winScroll > scrollHeight) {
            $('header').removeClass('header-active');
            $('header > ul').removeClass('header-active-main');
            $('header > ul').addClass('header-tit');

        } else {
            $('header').addClass('header-active');
            $('header > ul').removeClass('header-tit');
            $('header > ul').addClass('header-active-main');
        }
    });
});

//富文本字体控制(如果是异步的页面，请在接口内调用)
function richText (obj) {
    obj.each(function () {
        var dataLevel = $('html').attr('data-dpr');
        var _this = $(this);
        var fontSize =  parseInt(_this.attr('style').match(/(?:font-size:)*(\d+\d*)/));

        if (dataLevel == 2) {
            _this.css('font-size',2*fontSize);
        } else if (dataLevel == 3) {
            _this.css('font-size',3*fontSize);
        }
    });
}

//确认框构建
function comfirm(callback) {

    $('.sure-dialog-btn a:first-child').click(function(){
        $('.sure-dialog').fadeOut();
        if (typeof (callback) == 'function') {
            callback(false);
        }
    })

    $('.sure-dialog-btn a:last-child').click(function(){
        $('.sure-dialog').fadeOut();
        if (typeof (callback) == 'function') {
            callback(true);
        }
    })
}