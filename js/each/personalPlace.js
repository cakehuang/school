/**
 * Created by Fanjiang Huang on 2016/6/30.
 */

$(function(){

    /*地区选择*/

    $('.demo_treelist').mobiscroll().treelist({
        theme: 'mobiscroll',
        mode: 'scroller',
        display: 'bottom',
        lang: 'zh',
        rows:5,//滚动区域内的行数
        placeholder: '请选择省/市/区',
        headerText:function(valueText){return "省/市/区"}
    });

    $('.demo-cont').show();

})