/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-share': '&#xe90f;',
		'icon-join': '&#xe907;',
		'icon-click': '&#xe90a;',
		'icon-add': '&#xe900;',
		'icon-back': '&#xe901;',
		'icon-card': '&#xe902;',
		'icon-card-on': '&#xe903;',
		'icon-comment': '&#xe908;',
		'icon-in': '&#xe909;',
		'icon-more': '&#xe90b;',
		'icon-position': '&#xe90c;',
		'icon-select': '&#xe90d;',
		'icon-time': '&#xe90e;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
